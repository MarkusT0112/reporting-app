package com.example.reportingapp_;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ListActivity extends AppCompatActivity {
    public final static String EXTRA_CATEGORY = ListActivity.EXTRA_CATEGORY;
    public static String Categories = "Undefined";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Pothole Button Code
        Button potholeButton = findViewById(R.id.pothole);
        potholeButton.setOnClickListener(v -> {

            Categories = "Pothole";
            openReport();

        });

        //Graffiti Button Code
        Button graffitiButton = findViewById(R.id.graffiti);
        graffitiButton.setOnClickListener(v -> {

            Categories = "Graffiti";
            openReport();

        });

        //Illegal Dumping Button Code
        Button illegalDumpingButton = findViewById(R.id.illegal_dumping);
        illegalDumpingButton.setOnClickListener(v -> {

            Categories = "Illegal Dumping";
            openReport();

        });

        //Street Light Button Code
        Button streetLightButton =  findViewById(R.id.street_light);
        streetLightButton.setOnClickListener(v -> {

            Categories = "Street Light";
            openReport();

        });

        //Street Sign Button Code
        Button streetSignButton =  findViewById(R.id.street_sign);
        streetSignButton.setOnClickListener(v -> {

            Categories = "Street Sign";
            openReport();

        });


        //Missing Items Button Code
        Button missingObjectsButton =  findViewById(R.id.missing_objects);
        missingObjectsButton.setOnClickListener(v -> {

            Categories = "Missing Objects";
            openReport();

        });
        //Button to go back to MapsActivity on the location report screen
        Button buttonBackReport2 = (Button) findViewById(R.id.backButtonReport2);
        buttonBackReport2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLocationReport();

            }
        });


    }

    private void openLocationReport() {
        Intent intent = new Intent (this, LocationReport.class);
        startActivity(intent);
    }

    //Button to go back to MapsActivity on the location report screen


    //Main Passing Intent and open next Activity
    public void openReport() {

        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra(EXTRA_CATEGORY, Categories);
        startActivity(intent);
    }



}