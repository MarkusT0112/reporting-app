package com.example.reportingapp_;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationReport extends FragmentActivity implements OnMapReadyCallback {

        private GoogleMap mMap;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.location_report);
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);



             //Zoom in button
            Button zoomInButton = (Button) findViewById(R.id.zoomInButton);
            zoomInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    mMap.animateCamera(CameraUpdateFactory.zoomIn());
                }
            });

            //Zoom out button
            Button zoomOutButton = (Button) findViewById(R.id.zoomOutButton);
            zoomOutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMap.animateCamera(CameraUpdateFactory.zoomOut());
                }
            });


            //Button to go back to MapsActivity on the location report screen
            Button buttonBackReport = (Button) findViewById(R.id.backButtonReport);
            buttonBackReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openMapsActivity();

                }
            });


            //Button to go to Categories on the location report screen
            Button buttonNextReport = (Button) findViewById(R.id.nextButtonReport);
            buttonNextReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openListActivity();

                }
            });


        }


        //Method to go to MapsActivity
    public void openMapsActivity() {
        Intent intent = new Intent (this, MapsActivity.class);
        startActivity(intent);
    }


    //Method to go to List Screen
    public void openListActivity() {
        Intent intent = new Intent (this, ListActivity.class);
        startActivity(intent);
    }




        /**
             * Manipulates the map once available.
             * This callback is triggered when the map is ready to be used.
             * This is where we can add markers or lines, add listeners or move the camera. In this case,
             * we just add a marker near Sydney, Australia.
             * If Google Play services is not installed on the device, the user will be prompted to install
             * it inside the SupportMapFragment. This method will only be triggered once the user has
             * installed Google Play services and returned to the app.
             */

        //Puts marker on the map in Richmond,BC
        @Override
        public void onMapReady(GoogleMap googleMap) {
            googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(49.166592, -123.133568) ,13) );
            mMap = googleMap;
            Marker markerRichmond;

            // Add a marker in Richmond and move the camera
            final LatLng richmondLocation = new LatLng(49.166592, -123.133568);
            Marker richmond = mMap.addMarker(
                    new MarkerOptions()
                            .position(richmondLocation)
                            .draggable(true));
        }
    }

