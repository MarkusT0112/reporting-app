package com.example.reportingapp_;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Button for Map
        Button buttonMap = (Button) findViewById(R.id.buttonMaps);
        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLocationReport();

            }
        });



        }


        //Method to go to Location Report
    public void openLocationReport() {
        Intent intent = new Intent (this, LocationReport.class);
        startActivity(intent);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    @Override
    public void onMapReady(GoogleMap googleMap) {

        //Zooms in on Richmond when opening the page
        googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(49.166592, -123.133568) ,13) );

        mMap = googleMap;


        final LatLng richmondPotHole = new LatLng(49.166588, -123.133550);
        final LatLng richmondStreetlight = new LatLng(49.177099, -123.144571);

         //Add a marker in Richmond and move the camera
        mMap.addMarker(new MarkerOptions().position(richmondPotHole).title("2 Potholes in a 5km Radius"));
        mMap.addMarker(new MarkerOptions().position(richmondStreetlight).title("3 Broken Streetlights in a 5km Radius").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(richmondPotHole));


    }
}
