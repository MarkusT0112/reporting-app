package com.example.reportingapp_;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ReportActivity extends AppCompatActivity {

    private static final int GALLERY_REQUEST = 2;

    ImageButton btnPick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        //Category pass through code
        Intent intent = getIntent();
        String text = intent.getStringExtra(ListActivity.EXTRA_CATEGORY);

        TextView location = findViewById(R.id.location);

        location.setText(text);

        //Image Picker
        btnPick = findViewById(R.id.picture_selection);

        btnPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pngintent = new Intent();
                pngintent.setType(("image/*"));
                pngintent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(pngintent, "Pick An Image"), GALLERY_REQUEST);
            }
        });

        Button buttonBackReport3 = (Button) findViewById(R.id.backButtonReport3);
        buttonBackReport3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openListActivity();

            }
        });

        Button submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openThankYou();

            }
        });

    }

    private void openThankYou() {
        Intent intent = new Intent (this, ThankYou.class);
        startActivity(intent);
    }

    private void openListActivity() {
        Intent intent = new Intent (this, ListActivity.class);
        startActivity(intent);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri imageData = data.getData();

            btnPick.setImageURI(imageData);
        }

    }
}