package com.example.reportingapp_;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ThankYou extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thank_you);


        //Button to go back to the main home page
        Button nearYouButton = (Button) findViewById(R.id.nearYouButton);
        nearYouButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMapsActivity();

            }
        });


        //Button to go back to Location Report to start another report
        Button reportAgain = (Button) findViewById(R.id.reportAgainButton);
        reportAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLocationReport();

            }
        });
    }

    //Methods to open Location Report and Maps Activity

    public void openLocationReport() {
        Intent intent = new Intent (this, LocationReport.class);
        startActivity(intent);
    }

    public void openMapsActivity() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }

}
